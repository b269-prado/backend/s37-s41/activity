// courseRoute.js

const express = require("express");

const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");

// Route for creating a course
// S39 Activity - Refactor the course route to implement user authentication for the admin when creating a course.
/*
router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));	
});
*/

router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving ALL the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a SPECIFIC courses
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a course
router.put("/:courseId", (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

//S40 Activity 
/*
Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
*/
/*
router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});
*/
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
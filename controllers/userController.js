// userController.js

const User = require("../models/User");
const Course = require("../models/Course");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check if email already exist
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false
		};
	});
};

// UserRegistration
/*
BUSINESS LOGIC
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// "bcrypt.hashSync" is a function in the bcrypt library that used to generate hash value or a given input string synchronously
		// "reqBody.password" - input string that needs to be hashed
		// 10 - is value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt password
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// User Authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				// If password do not match
			} else {
				return false;
			}
		};
	});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};


// Authenticated user enrollment
module.exports.enroll = async (data) => {
	// Add course ID in the enrollment array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});
	// Add user ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

	if (isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	};
};











